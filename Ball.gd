extends KinematicBody2D

class_name Ball

signal goal_right
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

var player1_id: int;

var _speed: Vector2;
var new_speed: Vector2;

export var max_speed = 200
export var min_speed = 100

var bounce_count: int = 0


func set_speed(vect:Vector2):
	if vect.x >= 0:
		if vect.x < 64 : vect.x = 64
	else:
		if vect.x > -64 : vect.x = -64
	if vect.y >= 0:
		if vect.y < 32 : vect.y = 32
	else:
		if vect.y > -32 : vect.y = -32
	_speed = vect;

func random_speed() -> int:
	var speed_delta = max_speed - min_speed;
	return randi() % (2 * speed_delta) - speed_delta + min_speed

func randomize_speed():
	randomize();
	set_speed(Vector2(random_speed() * 16 / 9, random_speed()));

# Called when the node enters the scene tree for the first time.
func _ready() -> void:	
	reset()
	player1_id = get_parent().get_node("SnakeController/SnakeDrawer").get_instance_id();


func _physics_process(delta: float) -> void:
	var bounce_multiplier = float(bounce_count) / 10 + 1;
	new_speed = _speed * bounce_multiplier
	var collide: KinematicCollision2D = move_and_collide(new_speed * delta)
	if collide:
		if (collide.collider_id == player1_id):
			bounce_count = bounce_count + 1
		set_speed(_speed.bounce(collide.normal))
		
		
	if position.x > 1600 - 28 :
		
		emit_signal("goal_right")
		reset()
	
	
func reset():
	position = Vector2(128,128);
	bounce_count = 0;
	randomize_speed()
	







