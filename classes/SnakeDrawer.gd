extends TileMap

class_name SnakeDrawer

# A Node containing snake phisycs info
var snake: Snake

var toDestroy: Array = []

const SNAKE: int = 0;
const DESTRUCTION: int = 1;
const DESTROYED_TILE: Vector2 = Vector2(0,0)

const BROKEN_TILE = Vector2(7,0);

class Direction:
	const UP: Vector2 = Vector2(0,-1);
	const DOWN: Vector2 = Vector2(0,1);
	const LEFT: Vector2 = Vector2(-1,0);
	const RIGHT: Vector2 = Vector2(1,0);

class SnakeHeadTle:
	const UP: Vector2 = Vector2(0,0);
	const DOWN: Vector2 = Vector2(0,1);
	const LEFT: Vector2 = Vector2(1,0);
	const RIGHT: Vector2 = Vector2(1,1);
	
class SnakeBodyTile:
	const HORIZONTAL: Vector2 = Vector2(4,0);
	const VERTICAL: Vector2 = Vector2(4,1);
	const DR: Vector2 = Vector2(2,0);
	const DL: Vector2 = Vector2(3,0);
	const UR: Vector2 = Vector2(2,1);
	const UL: Vector2 = Vector2(3,1);

class SnakeTailTile:
	const UP: Vector2 = Vector2(5,0);
	const DOWN: Vector2 = Vector2(5,1);
	const LEFT: Vector2 = Vector2(6,0);
	const RIGHT: Vector2 = Vector2(6,1);
	
	
	
	

# func _init() -> void:
#	snake = _get_child_snake()

func _get_configuration_warning() -> String:
	if get_node("Snake"):
		return "You should provide a Snake child to this node" 
	return ""


func _get_sibling_snake():
	var desired_children = []
	var all_children = self.get_parent().get_children()
	for child in all_children:
		if child is Snake:
			desired_children.append(child)
	if desired_children.size() != 1:
		return null
	else:
		return desired_children[0]




# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	snake = _get_sibling_snake()
	draw_snake();
	
	
func draw_snake():
	delete_tiles(SNAKE);
	delete_tiles(DESTRUCTION);
	if toDestroy != []:
		for destroyedTile in toDestroy:
			set_cell(destroyedTile.x, destroyedTile.y, DESTRUCTION, false, false, false, DESTROYED_TILE);
		toDestroy = []		
		
	for i in snake.size():
		var block = snake.block(i);
		# head
		if i == 0:
			var tile = get_head_tile(relationTo(snake.block(1), block));
			set_cell(block.x, block.y, SNAKE, false, false, false, tile);
		elif i>0 && i<snake.size()-1:
			set_cell(block.x, block.y, SNAKE, false, false, false, 
					get_body_tile(snake.block(i-1), block, snake.block(i+1)));
		elif i == snake.size() - 1:
			set_cell(block.x, block.y, SNAKE, false, false, false, get_tail_tile(snake.block(i-1), block)); 
			
		else:
			set_cell(block.x, block.y, SNAKE, false, false, false, BROKEN_TILE);

func delete_tiles(id: int):
	for block in snake.body():
		for cell in get_used_cells_by_id(id):
			set_cell(cell.x, cell.y, -1)

func relationTo(first: Vector2, second: Vector2) -> Vector2:
	return second - first;

func any_match(a, b):
	return (a[0] == b[0] && a[1] == b[1]) || (a[0] == b[1] && a[1] == b[0])

func get_head_tile(direction: Vector2) -> Vector2:
	if direction == Direction.UP: return SnakeHeadTle.UP;
	if direction == Direction.DOWN: return SnakeHeadTle.DOWN;
	if direction == Direction.LEFT: return SnakeHeadTle.LEFT;
	if direction == Direction.RIGHT: return SnakeHeadTle.RIGHT;
	return BROKEN_TILE;

func get_body_tile(after: Vector2, this: Vector2, before: Vector2):
	var directions = [relationTo(this, after), relationTo(this, before)]
	if any_match(directions, [Direction.LEFT, Direction.RIGHT]): return SnakeBodyTile.HORIZONTAL
	if any_match(directions, [Direction.UP, Direction.DOWN]): return SnakeBodyTile.VERTICAL
	if any_match(directions, [Direction.DOWN, Direction.RIGHT]): return SnakeBodyTile.DR
	if any_match(directions, [Direction.DOWN, Direction.LEFT]): return SnakeBodyTile.DL
	if any_match(directions, [Direction.UP, Direction.RIGHT]): return SnakeBodyTile.UR
	if any_match(directions, [Direction.UP, Direction.LEFT]): return SnakeBodyTile.UL
	return BROKEN_TILE;

func get_tail_tile(after: Vector2, this: Vector2):
	var dir = relationTo(after, this);
	if dir == Direction.UP: return SnakeTailTile.UP
	if dir == Direction.DOWN: return SnakeTailTile.DOWN
	if dir == Direction.LEFT: return SnakeTailTile.LEFT
	if dir == Direction.RIGHT: return SnakeTailTile.RIGHT


func update():
	draw_snake();

func destroy(a: Array):
	toDestroy = a;

