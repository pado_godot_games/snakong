extends Node
class_name Snake


var _old_body: Array
var _body: Array
var direction: Vector2
var apple_collision: bool = false

var _walls_tile_map:TileMap = TileMap.new();


func _init(start_tiles = [
	Vector2(48,20),
	Vector2(48,21),
	Vector2(48,22),
	Vector2(48,23),
	Vector2(48,24),
	Vector2(48,25)
	]):
	_body =  start_tiles
	direction = Vector2.UP

func set_walls_tilemap(tilemap:TileMap):
	_walls_tile_map = tilemap;

func _ready() -> void:
	pass	
	
func move(direction: Vector2):
	var body_copy = _body.slice(0, _body.size() -1 if apple_collision else -2);	
	var new_head = body_copy[0] + direction;
	body_copy.insert(0, new_head)
	_old_body = _body
	_body = body_copy
	apple_collision = false

func collided_with_itself() -> Vector2:
	return head() if _body.count(block(0)) != 1 else null;

func collided_with_walls() -> Vector2:
	return head() if _walls_tile_map.get_used_cells().has(head()) else null;

func go_one_tick_back() -> void:
	_body = _old_body
	

func size() -> int:
	return _body.size()

func body():
	return _body

func block(i: int) -> Vector2:
	return _body[i]

func head() -> Vector2:
	return block(0)

func tail() -> Vector2:
	return block(size()-1)

func give_apple() -> void:
	apple_collision = true

func _shorten_to(size: int) -> Array:
	if size < 2: size = 2;
	var aux = _body.slice(size - 2, _body.size() - 2)
	_body = _body.slice(0, size -1)
	return aux;

func _cut_at(cutPoint: Vector2) -> Array:
	if size() <= 2:
		return []
	var aux: Array = []
	var reminder: Array = []
	var cutPointFound = false;
	for i in size():
		if i < 2: aux.push_back(block(i))
		else: 
			cutPointFound = cutPointFound || (block(i) == cutPoint)
			if cutPointFound: 
				reminder.push_back(block(i))
			else:
				aux.push_back(block(i))
	_body = aux
	return reminder

func big_hit() -> Array:
	deflect()
	return _shorten_to(size()*2/3)

func cut(cutPoint: Vector2) -> Array:
	return _cut_at(cutPoint)

func deflect() -> void:
	randomize()
	var r:int = randi() % 2;
	if direction == Vector2.UP || direction == Vector2.DOWN:
		if r == 0: direction = Vector2.LEFT
		else: direction = Vector2.RIGHT
	elif direction == Vector2.LEFT || direction == Vector2.RIGHT:
		if r == 0: direction = Vector2.UP
		else: direction = Vector2.DOWN
	if _walls_tile_map.get_used_cells().has(head()+direction):
		direction *= -1



func relationTo(first: Vector2, second: Vector2) -> Vector2:
	return second - first;

func get_block_direction(block: Vector2) -> Vector2:
	var b:int = body().find(block)
	return get_block_direction_by_index(b)
	
	
func get_block_direction_by_index(b:int) -> Vector2:
	if b == 0:
		if 1 > body().size():
			return Vector2(0,0)
		return relationTo(block(1), block(0))
	elif b == size()-1:
		return relationTo(block(b), block(b-1))
	else:
		var dir_towards_head = relationTo(block(b), block(b-1))
		var dir_from_tail = relationTo(block(b+1), block(b))
		if dir_towards_head == dir_from_tail:
			return dir_towards_head
		else: return dir_from_tail + dir_towards_head
	return Vector2(0,0)
