extends Node2D

class_name SnakeController

signal apple_eaten

# Declare member variables here. Examples:
var snake: Snake;
var snakeDrawer

var old_snake_direction;
var new_snake_direction = null;

var apple_pos: Vector2


func _get_child_snake():
	var desired_children = []
	var all_children = self.get_children()
	for child in all_children:
		if child is Snake:
			desired_children.append(child)
	if desired_children.size() != 1:
		return null
	else:
		return desired_children[0]

func _get_child_snakeDrawer():
	var desired_children = []
	var all_children = self.get_children()
	for child in all_children:
		if child is TileMap:
			desired_children.append(child)
	if desired_children.size() != 1:
		return null
	else:
		return desired_children[0] 

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	snake = _get_child_snake()
	snake.set_walls_tilemap(get_parent().get_node("Walls"))
	snakeDrawer = _get_child_snakeDrawer()
	old_snake_direction = snake.direction


func _input(event: InputEvent) -> void:
	if Input.is_key_pressed(KEY_Q):
		snake.give_apple()
	if Input.is_key_pressed(KEY_W):
		snakeDrawer.destroy(snake.big_hit())
		
	if new_snake_direction == null:
		if Input.is_action_just_pressed("ui_up") && old_snake_direction != Vector2(0,1):
			 new_snake_direction = Vector2(0,-1)
		if Input.is_action_just_pressed("ui_down") && old_snake_direction != Vector2(0,-1):
			 new_snake_direction = Vector2(0,1)
		if Input.is_action_just_pressed("ui_left") && old_snake_direction != Vector2(1,0):
			 new_snake_direction = Vector2(-1,0)
		if Input.is_action_just_pressed("ui_right") && old_snake_direction != Vector2(-1,0):
			 new_snake_direction = Vector2(1,0)
		if new_snake_direction != null && new_snake_direction != old_snake_direction * (-1):
			snake.direction = new_snake_direction
		






# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_Tick_timeout() -> void:
	old_snake_direction = snake.direction
	new_snake_direction = null
	
	snake.move(snake.direction)
	
	var collisionPoint = snake.collided_with_itself();
	if collisionPoint:
		snakeDrawer.destroy(snake.cut(collisionPoint))
	elif snake.collided_with_walls():
		snake.go_one_tick_back()
		snakeDrawer.destroy(snake.big_hit())
		snake.move(snake.direction)
	elif snake.head() == apple_pos:
		snake.give_apple()
		emit_signal("apple_eaten")
		
	
	#print(String(snake.head()) + " - " + String(apple_pos))
	
	snakeDrawer.update()
	
	var head_dir: Vector2 = snake.get_block_direction_by_index(1);
	if (head_dir == Vector2(0,0)):
		print("si è buggato")
	elif(head_dir == Vector2.UP):
		print("UP - - - " + String(head_dir))
	elif(head_dir == Vector2.DOWN):
		print("DOWN - - - " + String(head_dir))
	elif(head_dir == Vector2.LEFT):
		print("LEFT - - - " + String(head_dir))
	elif(head_dir == Vector2.RIGHT):
		print("RIGHT - - - " + String(head_dir))
	else:
		print("ANGLE - - - " + String(head_dir))
	


func _on_Apple1_apple_spawned(pos: Vector2) -> void:
	if snake.body().has(pos): emit_signal("apple_eaten")
	else: apple_pos = pos
	


