extends Area2D

class_name Apple

signal apple_spawned

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
export var max_x: int = 48
export var min_x: int = 28
export var min_y: int = 1
export var max_y: int = 26


func position_at_grid(x:int, y:int):
	position = Vector2(16,16) + (Vector2(x,y) * 32)
	emit_signal("apple_spawned", Vector2(x,y))

func position_at_random():
	randomize()
	var interval_x = max_x-min_x
	var interval_y = max_y-min_y
	var x:int = randi() % interval_x + min_x
	var y:int = randi() % interval_y + min_y
	position_at_grid(x,y)
	
	
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#position_at_grid(15,15)
	position_at_random()




func _on_SnakeController_apple_eaten() -> void:
	position_at_random()
