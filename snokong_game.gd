extends Node2D

onready var viewport = get_viewport().size

const SNAKE = 0;
const APPLE = 1;
const BORDER = 2;
const head: Vector2 = Vector2(0,0)
const body: Vector2 = Vector2(2,0)
var border_blocks;

var apple_pos: Vector2;
var apple_collision = false;
const SNAKE_BODY0 = [Vector2(5,10), Vector2(4,10), Vector2(3,10)]
const SNAKE_DIRECTION0 = Vector2(1,0)
var snake_body = SNAKE_BODY0
var snake_direction = Vector2(1,0)
var old_snake_direction = SNAKE_DIRECTION0;
var new_snake_direction = null;

const BROKEN_TILE = Vector2(7,0);

class Direction:
	const UP: Vector2 = Vector2(0,-1);
	const DOWN: Vector2 = Vector2(0,1);
	const LEFT: Vector2 = Vector2(-1,0);
	const RIGHT: Vector2 = Vector2(1,0);

class SnakeHeadTle:
	const UP: Vector2 = Vector2(0,0);
	const DOWN: Vector2 = Vector2(0,1);
	const LEFT: Vector2 = Vector2(1,0);
	const RIGHT: Vector2 = Vector2(1,1);
	
class SnakeBodyTile:
	const HORIZONTAL: Vector2 = Vector2(4,0);
	const VERTICAL: Vector2 = Vector2(4,1);
	const DR: Vector2 = Vector2(2,0);
	const DL: Vector2 = Vector2(3,0);
	const UR: Vector2 = Vector2(2,1);
	const UL: Vector2 = Vector2(3,1);

class SnakeTailTile:
	const UP: Vector2 = Vector2(5,0);
	const DOWN: Vector2 = Vector2(5,1);
	const LEFT: Vector2 = Vector2(6,0);
	const RIGHT: Vector2 = Vector2(6,1);
	
	


func _ready():
	reset()
	place_apple();
	border_blocks = $SnakeApple.get_used_cells_by_id(BORDER);
	



func place_apple():
	randomize();
	apple_pos = Vector2(randi() % 47 + 1, randi() % 25 + 1);
	if snake_body.has(apple_pos): place_apple();	
	
func draw_apple():
	$SnakeApple.set_cell(apple_pos.x, apple_pos.y, APPLE);



func headTile(direction: Vector2) -> Vector2:
	if direction == Direction.UP: return SnakeHeadTle.UP;
	if direction == Direction.DOWN: return SnakeHeadTle.DOWN;
	if direction == Direction.LEFT: return SnakeHeadTle.LEFT;
	if direction == Direction.RIGHT: return SnakeHeadTle.RIGHT;
	
	return Vector2(0,0);
	
		
func relationTo(first: Vector2, second: Vector2) -> Vector2:
	return second - first;

func any_match(a, b):
	return (a[0] == b[0] && a[1] == b[1]) || (a[0] == b[1] && a[1] == b[0])
	

func get_body_tile(after: Vector2, this: Vector2, before: Vector2):
	var directions = [relationTo(this, after), relationTo(this, before)]
	if any_match(directions, [Direction.LEFT, Direction.RIGHT]): return SnakeBodyTile.HORIZONTAL
	if any_match(directions, [Direction.UP, Direction.DOWN]): return SnakeBodyTile.VERTICAL
	if any_match(directions, [Direction.DOWN, Direction.RIGHT]): return SnakeBodyTile.DR
	if any_match(directions, [Direction.DOWN, Direction.LEFT]): return SnakeBodyTile.DL
	if any_match(directions, [Direction.UP, Direction.RIGHT]): return SnakeBodyTile.UR
	if any_match(directions, [Direction.UP, Direction.LEFT]): return SnakeBodyTile.UL
	return BROKEN_TILE;

func get_tail_tile(after: Vector2, this: Vector2):
	var dir = relationTo(after, this);
	if dir == Direction.UP: return SnakeTailTile.UP
	if dir == Direction.DOWN: return SnakeTailTile.DOWN
	if dir == Direction.LEFT: return SnakeTailTile.LEFT
	if dir == Direction.RIGHT: return SnakeTailTile.RIGHT
	
func draw_snake():
	for i in snake_body.size():
		var block = snake_body[i];
		# head
		if i == 0:
			var tile = headTile(relationTo(snake_body[1], block));
			$SnakeApple.set_cell(block.x, block.y, SNAKE, false, false, false, tile);
		elif i>0 && i<snake_body.size()-1:
			$SnakeApple.set_cell(block.x, block.y, SNAKE, false, false, false, 
					get_body_tile(snake_body[i-1], block, snake_body[i+1]));
		elif i == snake_body.size() - 1:
			$SnakeApple.set_cell(block.x, block.y, SNAKE, false, false, false, get_tail_tile(snake_body[i-1], block)); 
			
		else:
			$SnakeApple.set_cell(block.x, block.y, SNAKE, false, false, false, BROKEN_TILE);
	
		


func move_snake():
	delete_tiles(SNAKE);
	var body_copy = snake_body.slice(0, snake_body.size() -1 if apple_collision else -2);	
	var new_head = body_copy[0] + snake_direction;
	body_copy.insert(0, new_head)
	snake_body = body_copy
	apple_collision = false	
	

func delete_tiles(id: int):
	for block in snake_body:
		for cell in $SnakeApple.get_used_cells_by_id(id):
			$SnakeApple.set_cell(cell.x, cell.y, -1)

func detect_apple_collision() -> void:
	if snake_body[0] == apple_pos:
		apple_collision = true;
		delete_tiles(APPLE)
		place_apple()

func detect_game_over():
	# hit border
	if snake_body[0] in border_blocks:
		reset();
	# hit itself
	if snake_body.slice(1, snake_body.size() - 1).has(snake_body[0]):
		reset();
		
		
		
func reset():
	snake_body = [Vector2(5,10), Vector2(4,10), Vector2(3,10)]
	snake_direction = Vector2(1,0)
	


func _input(event: InputEvent) -> void:
	
	if new_snake_direction == null:
		if Input.is_action_just_pressed("ui_up") && old_snake_direction != Vector2(0,1):
			 new_snake_direction = Vector2(0,-1)
		if Input.is_action_just_pressed("ui_down") && old_snake_direction != Vector2(0,-1):
			 new_snake_direction = Vector2(0,1)
		if Input.is_action_just_pressed("ui_left") && old_snake_direction != Vector2(1,0):
			 new_snake_direction = Vector2(-1,0)
		if Input.is_action_just_pressed("ui_right") && old_snake_direction != Vector2(-1,0):
			 new_snake_direction = Vector2(1,0)
		if new_snake_direction != null && new_snake_direction != old_snake_direction * (-1):
			snake_direction = new_snake_direction



func _on_SnakeTick_timeout() -> void:
	old_snake_direction = snake_direction;
	new_snake_direction = null;
	move_snake();
	detect_apple_collision();
	draw_apple();
	detect_game_over();
	draw_snake();
	
	
