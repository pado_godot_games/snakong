extends Label


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

var points:int = 0;

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	self.text = String(points)




# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_Ball_goal_right() -> void:
	points = points + 1
	self.text = String(points)
